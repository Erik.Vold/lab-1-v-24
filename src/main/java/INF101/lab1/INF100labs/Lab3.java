package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        int n =49;
        int sum ;

        multiplesOfSevenUpTo(n);
        multiplicationTable(3);

        sum = crossSum(1);
        System.out.println(sum); // 1

        sum = crossSum(12);
        System.out.println(sum); // 3

        sum = crossSum(123);
        System.out.println(sum); // 6

        sum = crossSum(1234);
        System.out.println(sum); // 10

        sum = crossSum(4321);
        System.out.println(sum); // 10
    }

    public static void multiplesOfSevenUpTo(int n) {

        //throw new UnsupportedOperationException("Not implemented yet.");
        int antall = n/7 ;
        for ( int i= 0 ; i < antall  ; i++) {
            System.out.println(7*(i+1));
        }
    }

    public static void multiplicationTable(int n) {
        for ( int i= 1; i <= n ; i++) {
            System.out.print(i + ": " );
            for ( int k = 1 ; k <= n; k++){
                System.out.print(i*k + " ");
            }
            System.out.print("\n ");
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static int crossSum(int num) {
        //
        // throw new UnsupportedOperationException("Not implemented yet.");
        String streng = "" + num;
        int lengd;
        int sum = 0;
        int tall ;
        int itall;
        lengd = streng.length();
        for ( int i = 0 ; i < lengd ; i++) {
            tall = (int) streng.charAt(i);
            itall = tall % 48;
            sum += itall;
        }
        return sum;
    }

}
