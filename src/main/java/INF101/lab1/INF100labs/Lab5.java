package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> list1mult = new ArrayList<>(Arrays.asList(1,2,3,4));
        ArrayList<Integer> multipliedList1 = multipliedWithTwo(list1mult);
        System.out.println(multipliedList1);
        ArrayList<Integer> list2mult = new ArrayList<>(Arrays.asList(2, 2));
        ArrayList<Integer> multipliedList2 = multipliedWithTwo(list2mult);
        System.out.println(multipliedList1);
        ArrayList<Integer> list3mult = new ArrayList<>(Arrays.asList(0));
        ArrayList<Integer> multipliedList3 = multipliedWithTwo(list3mult);
        System.out.println(multipliedList1);
        ArrayList<Integer> removedList1 = new ArrayList<Integer>();
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        ArrayList<Integer> list2= new ArrayList<Integer>();
        List<Integer> unikList1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(1);
        list1.add(2);
        list1.add(1);
        list1.add(3);
        list1.add(3);
        list1.add(3);
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(2);
        list1.add(3);
        list1.add(3);
        list1.add(1);
        removedList1 =  removeThrees(list1);
        System.out.println(removedList1); // [1, 2, 4]

        System.out.println("unik");
        unikList1 = uniqueValues(list1);
        System.out.println(unikList1);
        System.out.println("unik slutt ");

        list2.add(2);
        list2.add(9);
        list2.add(56);
        list2.add(8);
        list2.add(9);
        list2.add(3);
        list2.add(5);
        list2.add(2);
        list2.add(9);
        list2.add(56);
        list2.add(8);
        list2.add(9);
        list2.add(3);
        list2.add(5);
        addList(list1, list2);
        System.out.println(list1 );
        System.out.println(list2);
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
      // throw new UnsupportedOperationException("Not implemented yet.");
       ArrayList<Integer> result = new ArrayList<>();
       for ( int i=0; i < list.size() ; i++ ){
           result.add(i, 2*list.get(i));
       }
       return result;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
       // throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> aList = new ArrayList<Integer>();
        int element;
        for (int i = 0; i <list.size() ; i++) {
            element = list.get(i);
            if ( element != 3 ) {
                aList.add(element);
            }
        }
        return aList;

    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
      //  throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> aList = new ArrayList<Integer>();
        int element;
        int element2;
        boolean found = false;
        for (int i = 0; i <list.size() ; i++) {
            element = list.get(i);
            for (int k=0; k< aList.size(); k++ ) {
                element2 = aList.get(k);
                if (element == element2 ) {
                    found = true;
                    break;
                } else {
                    found = false;
                }
            }
            if (!found ) {
                aList.add(element);
            }
        }
        return aList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
      //  throw new UnsupportedOperationException("Not implemented yet.");
        int aValue;
        int bValue;
        for ( int i=0 ; i < a.size() ; i++) {
            aValue = a.get(i);
            bValue = b.get(i);
            a.set(i, aValue + bValue);
        }
    }

}
