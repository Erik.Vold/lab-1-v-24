package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        ArrayList<Integer> row1   = new ArrayList<>();
        ArrayList<Integer> row2  = new ArrayList<>();
        ArrayList<Integer> row3  = new ArrayList<>();
        row1.add(11);
        row1.add(12);
        row1.add(13);
        row2.add(12);
        row2.add(13);
        row2.add(11);
        row3.add(13);
        row3.add(11);
        row3.add(12);
        grid1.add(row1);
        grid1.add(row2);
        grid1.add(row3);

        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }

        System.out.println();
        boolean equalSums = allRowsAndColsAreEqualSum(grid1);
        System.out.println(equalSums);
        System.out.println();
        removeRow(grid1, 1);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
     //   throw new UnsupportedOperationException("Not implemented yet.");
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
       // throw new UnsupportedOperationException("Not implemented yet.");
        ArrayList<Integer> delListe = new ArrayList<Integer>();
        boolean result = false;
        int sum = 0;
        int test;
        boolean resultat = true;

        delListe = grid.get(0);
        for  ( int idel= 0; idel < delListe.size(); idel++ ) {
            sum += delListe.get(idel);
        }

        for ( int i= 0;i < grid.size() ; i++) {
            test = 0;
            delListe = grid.get(i);
            for ( int j= 0; j < delListe.size(); j++ ) {
                test += delListe.get(j);

            }

            if (test != sum ) {
                resultat = false;
            }

        }

        delListe = grid.get(0);
        for ( int k = 0; k < delListe.size() ; k++) {
            test = 0;

            for ( int kk = 0; kk <  grid.size(); kk++ ) {
                test += grid.get(kk).get(k);

            }

            if (test != sum ) {
                resultat = false;
            }
        }
        return resultat;
    }

}
