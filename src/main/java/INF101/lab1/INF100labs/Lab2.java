package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        findLongestWords("Game", "Action", "Champion") ;
        findLongestWords("apple", "carrot", "ananas");
        findLongestWords("Four", "Five", "Nine");

        System.out.println("2022  " + isLeapYear(2022));
        System.out.println("1996  " + isLeapYear(1996));
        System.out.println("1900  " + isLeapYear(1900));
        System.out.println("2000  " + isLeapYear(2000));

        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false
    }

    public static void findLongestWords(String word1, String word2, String word3) {
       // throw new UnsupportedOperationException("Not implemented yet.");
        int len0 = 0;
        int len1 = word1.length();
        int len2 = word2.length();
        int len3 = word3.length();
        if ( len1 >= len2 ) {
            len0 = len1;
        } else {
            len0 = len2;
        }
        if ( len3 >= len0) {
            len0 = len3;
        }
        if ( len0 == len1 ) {
            System.out.println(word1);
        }

        if ( len0 == len2 ) {
            System.out.println(word2);
        }
        if ( len0 == len3 ) {
            System.out.println(word3);
        }
        return;
    }

    public static boolean isLeapYear(int year) {

     //  throw new UnsupportedOperationException("Not implemented yet.");
        if ( ( year % 100) == 0 && ((year % 400) == 0) ) {
            return true;
        } else if ( ( year % 100) == 0 ) {
            return false;
        }
        else if ( ( year % 4 ) == 0 ) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
      //  throw new UnsupportedOperationException("Not implemented yet.");
        if ( num > 0 && num % 2 == 0 ) {
            return true;
        } else {
            return false ;
        }
    }

}
